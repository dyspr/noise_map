var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 9
var array = create2DArray(arraySize, arraySize, 0, false)
var initSize = 0.07
var steps = create2DArray(arraySize, arraySize, 0, true)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight, WEBGL)
}

function draw() {
  createCanvas(windowWidth, windowHeight, WEBGL)
  ortho(-width/2, width/2, -height/2, height/2, 0, boardSize * 2)
  background(0)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  rotateX(Math.PI * 1 / 4 + frameCount * 0.075)
  rotateZ(Math.PI * 0.314 + frameCount * 0.025)
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      noFill(array[i][j] * 255 / 3)
      stroke(255)
      strokeWeight(2)
      push()
      translate((i - Math.floor(arraySize * 0.5)) * boardSize * initSize, (j - Math.floor(arraySize * 0.5)) * boardSize * initSize, array[i][j] * boardSize * initSize * 3 * 0.5 - boardSize * 0.1)
      box(boardSize * initSize * array[i][j] * (1 / 2.5), boardSize * initSize * array[i][j] * (1 / 2.5), array[i][j] * boardSize * initSize)
      pop()

      var xNoise = noise(steps[i][j])
      var x = map(xNoise, 0, 1, 0, 2.5)

      array[i][j] = x
      steps[i][j] += 0.1 * Math.random()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight, WEBGL)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.random()
      }
    }
    array[i] = columns
  }
  return array
}
